# Readme

<a href="https://www.buymeacoffee.com/mezantrop" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Coffee" height="41" width="174"></a>

## About

**Supermatic** is a simple HTTP server I wrote just for fun as Bourne Shell script. It supports minimal features and should not be considered for use in production.  

#### There are no warranties. Don't expect it ~~to work~~ to be secure or bug free!

* * *

## Supported features and standards

**Supermatic** is more or less POSIX compliant, it can run on FreeBSD and on any other OS which has Bourne Shell and inetd-like daemon.
**Supermatic** serves GET requests for HTML, TXT, and PHP resources. It prertty well handles GIF, JPEG, and PNG image formats, as well as directory listing option.
**Supermatic** also supports basic access authentication. But what's about any other standards and features? No, never heard about them.

* * *

## Installation and configuration

*   Download [**Supermatic**](https://gitlab.com/mezantrop/supermatic/-/archive/master/supermatic-master.tar.gz) and unarchive it into the desired directory
*   At least, change the _RootDir_ variable in the _smhttpd.conf_ file
*   Populate _www_ directory with your own content
*   Change the owner of the RootDir and all subdirectories (user _nobody_ is a good choice for it)
*   Add someting like that to _/etc/inetd.conf_:

    <pre>smhttpdsh	stream	tcp	nowait		nobody	/usr/local/supermatic/smhttpd.sh smhttpd.sh /usr/local/supermatic/smhttpd.conf</pre>

*   And this line to _/etc/services_:

    <pre>smhttpdsh		8080/tcp</pre>

*   Configure and start/restart inetd daemon
*   Access the server at: _http://your-server-address:8080_. To troubleshoot and diagnose check logs in the _log_ directory
*   Configure external log rotation of _access.log_ and _server.log_ files since their growth is not controlled by **Supermatic**

* * *

## TODO

- [ ]  All other modern features and options

There are a lot of things to do. I implement them all, someday in the future.
Probably not ;)

* * *

## License and warranties

**There are no warranties at all!**

**Supermatic** is licensed under the [BEER-WARE License](license.txt)

* * *

## FAQ

**Q:** Is it possible to run **Supermatic** as a standalone "daemon"?

**A:** Yes, on some OS **Supermatic** can be started in standalone mode
utilizing _/dev/tcp_ facility or using _netcat_. If you want _netcat'ed_ version,
contact [me](mailto:zmey20000@yahoo.com), because it's outdated and has
issues with muliplexing connections.

**Q:** What's up? Your pages look like they came from 2000!

**A:** No surprises, it's because I wrote them in pure _vi_ editor.

* * *

_Supermatic by [Mikhail Zakharov](mailto:zmey20000@yahoo.com), 2018, 2020, [BEER-WARE License](license.txt)_
