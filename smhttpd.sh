#!/bin/sh

# Supermatic - a simple HTTP server I wrote just for fun as Bourne Shell script

# -----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <zmey20000@yahoo.com> wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return.    Mikhail Zakharov
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Changelog
# -----------------------------------------------------------------------------
# 2018.11.17    v0.5    	The first public release
# 2018.11.18    v0.5.1  	MIME types update
# 2020.05.15    v0.5.2  	Syntax cleaning
# 2020.05.16	v0.6		Configuration file, AccessLog enhancements
# 2020.05.20	v0.7		Directory listing, HEAD method
# 2020.07.11	v0.7.1		Fixed 501 Error handling
# 2020.07.18	v0.7.2		Fixes URL compilation and directory listing
# 2020.07.19	v0.8.0		Basic access authentication
# 2020.07.19	v0.8.1		Exclude files from directory listing
# 2020.07.19	v0.8.2  	Directory samples and index.html update
# 2020.07.19	v0.8.3  	Busybox workaround for ls time formatting
# 2020.07.20	v0.8.3.1	stat to follow symlinks
# 2020.07.20	v0.8.3.2	Busybox detection fix; URL processing fix
# 2020.07.21	v0.9		PHP support added
# 2020.07.21	v0.10		SSL support; Darwin "ls" via gls from coreutils
# 2020.10.30	v0.11		Initial CGI-BIN
# 2020.11.02	v0.11.1		hostname bug in OpenWRT fix
# 2020.11.02	v0.11.2		Getpeer rollback
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Default configurable variables
# -----------------------------------------------------------------------------
RootDir="/usr/local/supermatic"
DocumentRoot="$RootDir""/www"
DocumentError="$RootDir""/error"

cgibin="cgi-bin"

ServerLog="$RootDir""/log/server.log"
AccessLog="$RootDir""/log/access.log"
AccessLog_full="Yes"			# Log all requet headers if "yes" or...
AccessLog_fields="Host Referer"		# ...log these fields if "no"

IndexFile="index.html"
htaccess=".htaccess"

ConnectionTimeout=3			# Connection timeout in seconds
ServerName=`which hostname >/dev/null && hostname || echo "Unnamed"`

[ $SSL_port ] && ServerPort=$SSL_port || ServerPort=80

ServerString="Supermatic v0.8.3"
ServerProtocol="HTTP/1.1"
GatewayInterface="CGI/1.1"

LsExclude="$htaccess|passwd|.htpasswd"	# Patterns to exclude from ls directory

openssl="openssl"			# OpenSSL binary to use
php="php-cgi"				# PHP executable

# -----------------------------------------------------------------------------
tolower() {
	# Transfor argument to low case or print "\n"
	[ -n "$1" ] &&
		printf "%s" "$1" | tr '[:upper:]' '[:lower:]' ||
		printf "\n"
}

# Override default variables using configurtion file
[ -n "$1" -a -r "$1" ] && . "$1"

REMOTE_ADDR=${SOCAT_PEERADDR-""}
REMOTE_PORT=${SOCAT_PEERPORT-""}

cd "$DocumentRoot"

# Redirect errors to the special file
exec 2>> $ServerLog

alias date='date "+%Y-%m-%d %H:%M:%S"'
case `uname -s` in
	*BSD)
		alias sed='sed -E'
		alias stat='stat -L -f "%z"'
		alias ls='ls -lA -D "%Y-%m-%d %H:%M:%S"'
		;;
	Darwin)
		alias sed='sed -E'
		alias stat='stat -L -f "%z"'
		alias ls='gls -lA --time-style=+"%Y-%m-%d %H:%M:%S"'
		;;
	Linux)
		alias sed='sed -r'
		alias stat='stat -L -c "%s"'
		alias ls='ls -lA --time-style=+"%Y-%m-%d %H:%M:%S"'
		;;
	*)
		printf "Fatal! Unsupported OS detected\n"
esac
[ -h "/bin/ls" -a "`readlink /bin/ls`" = "busybox" ] && {
	busybox=1 ;
	alias ls='ls -lA --full-time' ;
} || busybox=0

reply_header() {
	# Reply header - $1: STATUS; $2: MIME FILE EXTENTION
	# $3: SIZE in bytes; $4: ADDITIONAL HEDERS

	printf "%s %s\n" "$ServerProtocol" "$1"
	printf "Server: %s\n" "$ServerString"
	printf "Content-length: %d\n" "$3"
	printf "Connection: close\n"
	[ -n "$4" ] && printf "%s\n" "$4"

	case "$2" in
		*.cgi) ;;
		*.[cC][sS][sS])
			mime_type="text/css" ;;
		*.[gG][iI][fF])
			mime_type="image/gif" ;;
		*.[hH][tT][mM]|*.[hH][tT][mM][lL])
			mime_type="text/html" ;;
		*.[jJ][pP][gG]|*.[jJ][pP][eE]|*.[jJ][pP][eE][gG])
			mime_type="image/jpeg" ;;
		*.[jJ][sS])
			mime_type="text/javascript" ;;
		*.[pP][hH][pP]|*.[pP][hH][pP][3-9]|*.[pP][hH][tT][mM][lL])
			mime_type="text/html" ;;
		*.[pP][nN][gG])
			mime_type="image/png" ;;
		*)
			mime_type="text/plain; charset=us-ascii" ;;
	esac

	[ "$2" != ".cgi" ] && {
		printf "Content-Type: %s\n" "$mime_type";
		printf "\n";
	}
}

reply_401() {
	# Reply "401 Unauthorized" error. $1 - Realm string
	[ -n "$1" ] && realm="$1" || realm=""

	eurl="$DocumentError/401.html"
	auth_header="WWW-Authenticate: Basic realm=\"$realm\""
	reply_header "401 Unauthorized" "$eurl" `stat "$eurl"` "$auth_header"
	cat "$eurl"
}

lsdir() {
	# List $1 directory contents or show Error 404

	[ -n "$1" ] && {
		[ $busybox -eq 1 ] &&
			ls_dir=`ls "$1" | awk '{print \$1,\$2,\$3,\$4,\$5,\$6,\$7,\$9}'` ||
			ls_dir=`ls "$1"`;

		dir=`printf "%s" "$ls_dir" | grep -E -v "$LsExclude" |
			awk -v url="/$1" '
				/^d|^-/ {
					print "\t\t\t<TR>\n\
\t\t\t\t<TD><A HREF=\""url"/"$8"\">"$8"</A></TD>\
<TD>"$3":"$4"</TD>\
<TD>"$5" bytes</TD><TD>"$6,$7"</TD>\n\
\t\t\t</TR>"}'` ;

		printf '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">\n<HTML>\n\t<BODY>
\t\t<H2>%s</H2>\n\t\t<HR>
\t\t<TABLE width=100%%; style="font-family: Monospace">\n%s
\t\t</TABLE>\n\t\t<HR><i>Generated by %s at %s</i>\n
\t</BODY>\n</HTML>' "$1" "$dir" "$ServerString" "`date`";
	} || {
		eurl="$DocumentError/404.html" ;
		reply_header "404 Not Found" "$eurl" `stat "$eurl"` ;
		cat "$eurl" ;
	}
}

url_decode() {
	# Basic URL decode: just tokenize the query string $1

	printf "%s\n" "$1" |
		awk '
			BEGIN {FS="[\&\?]"; ORS=" "};
			{print substr($1,2); for (f=2; f<=NF; f++) print($f)}'
}

read -t $ConnectionTimeout -r request url version || exit 2;
# Log the request header
while read -t 1 req_field; do
	# Single '\r' or '\n' denotes the end of the header
	[ "$req_field" = $'\x0d' -o "$req_field" = $'\x0a' ] && break

	# Check for Authorization field in the request header
	case `tolower "$req_field"` in
		*`tolower "Authorization"`:*)
			authorization="$req_field"
			;;
	esac

	# Log the request header
	case "$AccessLog_full" in
		[nN][oO])
			# Log selected only fields
			for fld in $AccessLog_fields ; do
				case `tolower "$req_field"` in
					*`tolower "$fld"`:*)
						req_header="$req_header""$req_field"
						;;
				esac
			done
			;;
		[yY][eE][sS]|*)
			# Log all request header fields
			req_header="$req_header""$req_field"
			;;
	esac
done

printf "Timestamp: %s\nRequest: %s%sRemote: %s:%s\n\n" \
	"`date`" "$request $url $version" "$req_header" "$REMOTE_ADDR" "$REMOTE_PORT" |
	tr '\r' '\n' >> $AccessLog

# Parse the requested URL mapped to the CWD
url="."`printf "%s\n" "$url" | cut -f 2 -d ' ' |
	sed 's/\.+/./g; s/\"\<\>\#\%\{\}\|\\^\~\[\]\`//g'`
# Append Index file if it exists in the requested directory
# Extra "/" ensures the path will be translated correctly
[ -d "$url" -a -f "$url/$IndexFile" ] && url="$url/$IndexFile"

# Process htaccess file
[ -f "$url"/"$htaccess" ] &&
	htaccess="$url"/"$htaccess" ||
	htaccess=`dirname "$url"`/"$htaccess"

if [ -f "$htaccess" ]; then
	realm=`cat "$htaccess" | grep realm | cut -d '=' -f 2`
	if [ -z "$authorization" ]; then
		# Request authorization
		reply_401 "$realm"
		exit 0
	else
		# Check submitted username:password
		auth_request=`printf "%s\n" "$authorization" |
			cut -d ' ' -f 3 | "$openssl" enc -base64 -d`
		[ `grep "$auth_request" "$htaccess"` ] ||
			{ reply_401 "$realm"; exit 0; }
	fi
fi

case "$request" in
	"GET"|"HEAD")
		# Serving a file
		if [ -e "$url" -a -f "$url" ]; then
			case "$url" in
				*.[pP][hH][pP]|*.[pP][hH][pP][3-9]|*.[pP][hH][tT][mM][lL])
					content=`"$php" -f "$url"`
					reply_header "200 OK" ".php" `printf "%s" "$content" | wc -c`
					[ "$request" == "GET" ] && printf "%s" "$content"
					;;
				./*"$cgibin"/*)
					# Very basic CGI-BIN implementation, not fully compliant to RFC3875
					# no query string allowed and etc.
					script_name="`basename $url`"		# TODO: Add relative path

						# PATH_INFO="" \		# These
						# PATH_TRANSLATED="" \		# variables
						# QUERY_STRING="" \		# are not
						# REMOTE_HOST="" \		# implemented
						# AUTH_TYPE="" \		# in
						# REMOTE_USER="" \		# the
						# REMOTE_IDENT="" \		# current
						# CONTENT_TYPE="" \		# version
						# CONTENT_LENGTH="" \		# yet

					content=`/usr/bin/env -i \
						SERVER_SOFTWARE="$ServerString" \
						SERVER_NAME="$ServerName" \
						GATEWAY_INTERFACE="$GatewayInterface" \
						SERVER_PROTOCOL="$ServerProtocol" \
						SERVER_PORT="$ServerPort" \
						REQUEST_METHOD="$request" \
						SCRIPT_NAME="$script_name" \
						REMOTE_ADDR="$REMOTE_ADDR":"$REMOTE_PORT" \
						$url`
					reply_header "200 OK" ".cgi" `printf "%s" "$content" | wc -c`
					[ "$request" == "GET" ] && printf "%s" "$content"
					;;
				*)
					reply_header "200 OK" "$url" `stat "$url"`
					[ "$request" == "GET" ] && cat "$url"
					;;
			esac
		# Serving a directory
		elif [ -e "$url" -a -d "$url" ]; then
			dir=`lsdir "$url"`
			reply_header "200 OK" ".html" `printf "%s" "$dir" | wc -c`
			[ "$request" == "GET" ] && printf "%s" "$dir"
		# URL Not found
		else
			eurl="$DocumentError/404.html"
			reply_header "404 Not Found" "$eurl" `stat "$eurl"`
			[ "$request" == "GET" ] && cat "$eurl"
		fi
		;;
	*)
		eurl="$DocumentError/501.html"
		reply_header "501 Not Implemented" "$eurl" `stat "$eurl"`
		cat "$eurl"
	;;
esac
