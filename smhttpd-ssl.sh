#!/bin/sh

RootDir='/data/supermatic'
export SSL_port=443
SSL_certificate="$RootDir/smhttpd-cert.pem"
SSL_key="$RootDir/smhttpd-key.pem"
SSL_runas="nobody"

ServerLog="$RootDir""/log/server.log"

# Override default variables using configurtion file
[ -n "$1" -a -r "$1" ] && . "$1"

socat openssl-listen:"$SSL_port",cert="$SSL_certificate",key="$SSL_key",\
verify=0,reuseaddr,fork,"su=$SSL_runas" \
exec:"$RootDir/smhttpd.sh $RootDir/smhttpd.conf" 2>>$ServerLog &

